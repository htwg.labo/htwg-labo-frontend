import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ErrorService {

  public error$$ = new BehaviorSubject(false);

  public error$ = this.error$$.asObservable();

  constructor() { }
}
