import { Injectable, OnDestroy } from '@angular/core';
import { UserManagementService } from '@shared';
import { User } from '@shared';
import { ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserManagementCacheService implements OnDestroy{

  private _subs = new Subscription();
  private _user$$ = new ReplaySubject<User | null>(1);

  public user$ = this._user$$.asObservable();
  
  constructor(
    private _userManagementService: UserManagementService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public subscribeToUser():void{
    this._subs.add(
      this._userManagementService.getUser().subscribe((user)=>{
        this._user$$.next(user);
      })
    );
  }
}
