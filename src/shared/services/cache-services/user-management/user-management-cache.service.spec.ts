import { TestBed } from '@angular/core/testing';

import { UserManagementCacheService } from './user-management-cache.service';

describe('UserManagementCacheService', () => {
  let service: UserManagementCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UserManagementCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
