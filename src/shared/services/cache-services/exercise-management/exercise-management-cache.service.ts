import { Injectable, OnDestroy } from '@angular/core';
import { LoadingService } from '@shared';
import { ExerciseManagementService } from '@shared';
import { Exercise } from '@shared';
import { map, ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExerciseManagementCacheService implements OnDestroy {

  private _subs = new Subscription();
  private _lecturerExercises$$ = new ReplaySubject<Exercise[]>(1);
  private _studentExercises$$ = new ReplaySubject<Exercise[]>(1);

  public lecturerExercises$ = this._lecturerExercises$$.asObservable();
  public studentExercises$ = this._studentExercises$$.asObservable();
  
  constructor(
    private _exerciseManagementService: ExerciseManagementService,
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public subscribeToStudentExercises():void{
    this._subs.add(
      this._exerciseManagementService.getStudentExercises()
      .pipe(
        map((studentExercises) => {
          const exercisesWithSteps = studentExercises.filter((exercise)=>exercise.hasDefinedSteps === true)
          return exercisesWithSteps;
        })
      )
      .subscribe((studentExercises)=>{
        this._studentExercises$$.next(studentExercises);
      })
    );
  }

  public subscribeToLecturerExercises():void{
    this._subs.add(
      this._exerciseManagementService.getLecturerExercises().subscribe((lecturerExercises)=>{
        this._lecturerExercises$$.next(lecturerExercises);
      })
    );
  }
}
