import { TestBed } from '@angular/core/testing';

import { ExerciseManagementCacheService } from './exercise-management-cache.service';

describe('ExerciseManagementCacheService', () => {
  let service: ExerciseManagementCacheService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExerciseManagementCacheService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
