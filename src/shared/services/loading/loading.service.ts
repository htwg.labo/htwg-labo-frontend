import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  private _loading$$ = new BehaviorSubject(false);
  private _numberOfLoadingInstances = 0;

  public loading$ = this._loading$$.asObservable();

  constructor() { }

  public loading(isLoading:boolean){
    if(isLoading){
      this._numberOfLoadingInstances = this._numberOfLoadingInstances + 1;
    } else {
      this._numberOfLoadingInstances = this._numberOfLoadingInstances - 1;
    }
    if(this._numberOfLoadingInstances <= 0){
      this._loading$$.next(false);
    } else{
      this._loading$$.next(true);
    }
  }
}
