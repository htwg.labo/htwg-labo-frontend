import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '@env';
import { ErrorService, User } from '@shared';
import { LoadingService } from '@shared';
import { AuthService } from '@shared';
import { catchError, Observable, of, ReplaySubject, Subscription, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserManagementService implements OnDestroy{

  private _subs = new Subscription();
  private _errorText = '';
  
  constructor(
    private _authService: AuthService,
    private _loadingService: LoadingService,
    private _errorService: ErrorService,
    private _httpClient: HttpClient, 
    private _snackBar: MatSnackBar
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  // Get user data from backend
  public getUser():Observable<User | null>{
    const loginToken = this._authService.getToken();
    this._loadingService.loading(true);
    this._errorService.error$$.next(false);
    return this._httpClient.get<User>(environment.backendBaseUrl + '/user-management/getUser?token=' + loginToken.token).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Login ist fehlgeschlagen. Versuche es nochmal!'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of(null);
          }),
          tap(() => {
            this._loadingService.loading(false);
          })
      )
  }

  // Get all participants of a moodle course
  public getCourseParticipants(courseId: number):Observable<User[]>{
    const loginToken = this._authService.getToken();
    this._loadingService.loading(true);
    this._errorService.error$$.next(false);
    return this._httpClient.get<User[]>(environment.backendBaseUrl + '/user-management/getCourseParticipants?' + 'courseId=' + courseId + '&token=' + loginToken.token).pipe(
          catchError(error => {
            switch(error.status) { 
              case 401: { 
                this._errorText = 'Login ist fehlgeschlagen. Versuche es nochmal!'
                this._errorService.error$$.next(true);
                break; 
              } 
              case 0: { 
                this._errorText = 'Server Error: ' + error.message
                break; 
              } 
              default: { 
                this._errorText = 'Unknown error'
                  break; 
              } 
            } 
            this._snackBar.open(this._errorText, undefined, {
              duration: 5000,
              panelClass: ['red-snackbar'],
            });
            this._errorText='';
            return of([]);
          }),
          tap(() => {
            this._loadingService.loading(false);
          })
      )
  }
}
