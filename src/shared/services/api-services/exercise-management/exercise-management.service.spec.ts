import { TestBed } from '@angular/core/testing';

import { ExerciseManagementService } from './exercise-management.service';

describe('ExerciseManagementService', () => {
  let service: ExerciseManagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExerciseManagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
