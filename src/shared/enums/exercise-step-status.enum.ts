export enum ExerciseStepStatusEnum {
    TODO = 'TODO',
    DOING = 'DOING',
    DONE = 'DONE',
    PROBLEM = 'PROBLEM',
}