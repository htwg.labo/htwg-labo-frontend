export interface User{
    username: string;
    firstname: string;
    lastname: string;
    userid: number;
    userpictureurl: string;
}
