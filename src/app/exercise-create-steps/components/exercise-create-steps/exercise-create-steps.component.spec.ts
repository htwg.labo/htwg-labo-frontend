import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseCreateStepsComponent } from './exercise-create-steps.component';

describe('ExerciseCreateStepsComponent', () => {
  let component: ExerciseCreateStepsComponent;
  let fixture: ComponentFixture<ExerciseCreateStepsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseCreateStepsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseCreateStepsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
