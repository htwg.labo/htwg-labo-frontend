import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { moveItemInArray} from '@angular/cdk/drag-drop';
import { ExerciseManagementCacheService, ExerciseManagementService, ExerciseStep, ExerciseWithSteps, LoadingService } from '@shared';
import { ExerciseCreateStepsService } from '../../services/exercise-create-steps.service';
@Component({
  selector: 'app-exercise-create-steps',
  templateUrl: './exercise-create-steps.component.html',
  styleUrls: ['./exercise-create-steps.component.scss']
})
export class ExerciseCreateStepsComponent implements OnInit{

  public showCreateModal = false;
  public loading$ = this._loadingService.loading$;
  public lecturerExercise$ = this._exerciseCreateStepsService.lecturerExercise$;
  public exerciseSteps: ExerciseStep[] = [
    {
      name: '',
      description: '',
      order: 0
    }
  ];

  private _exerciseId!:number;

  constructor(
    private _router: Router,
    private _exerciseCreateStepsService: ExerciseCreateStepsService,
    private _exerciseManagementService: ExerciseManagementService,
    private _exerciseManagementCacheService: ExerciseManagementCacheService,
    private _route: ActivatedRoute,
    private _loadingService: LoadingService
  ) {}

  ngOnInit(): void {
    const exerciseId = this._route.snapshot.params['exerciseId'];
    if(exerciseId){
      const id: number = +exerciseId;
      this._exerciseId = id;
      this._exerciseCreateStepsService.subscribeToLecturerExercise(id);
    }
  }

  public navigateToHome():void{
    this._router.navigate(['/home']);
  }

  public dropExerciseStep(event:any):void {
    moveItemInArray(this.exerciseSteps, event.previousIndex, event.currentIndex);
    this._updateStepOrders();
  }

  public removeExerciseStep(index: number):void {
    if(index !== -1){
      this.exerciseSteps.splice(index, 1);
      this._updateStepOrders();
    }
  }

  public updateStepName(stepName: string, index: number):void{
    if(index !== -1){
      this.exerciseSteps[index].name = stepName;
    }
  }

  public updateStepDescription(stepDescription: string, index: number):void{
    if(index !== -1){
      this.exerciseSteps[index].description = stepDescription;
    }
  }

  public addStep():void{
    this.exerciseSteps.push({
      name: '',
      description: '',
      order: (this.exerciseSteps.length)
    });
  }

  public createExerciseWithSteps():void{
    const exerciseWithSteps: ExerciseWithSteps = {
      exerciseId: this._exerciseId,
      steps: this.exerciseSteps
    }
    this._exerciseManagementService.addNewExerciseWithSteps(exerciseWithSteps);
    this._exerciseManagementCacheService.subscribeToLecturerExercises();
  }

  private _updateStepOrders():void{
    this.exerciseSteps.forEach((exercise, i)=>{
        exercise.order = i;
      }
    );
  }
}
