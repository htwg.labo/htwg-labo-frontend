import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExerciseCreateStepsComponent } from './components/exercise-create-steps/exercise-create-steps.component';
import { ButtonModule, HeaderModule, InputModule, LoadingModule, ModalModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';
import { DragDropModule } from '@angular/cdk/drag-drop';



@NgModule({
  declarations: [
    ExerciseCreateStepsComponent
  ],
  imports: [
    CommonModule,
    HeaderModule,
    TranslateModule,
    LoadingModule,
    DragDropModule,
    ButtonModule,
    InputModule,
    ModalModule
  ]
})
export class ExerciseCreateStepsModule { }
