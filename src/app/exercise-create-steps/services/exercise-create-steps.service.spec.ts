import { TestBed } from '@angular/core/testing';

import { ExerciseCreateStepsService } from './exercise-create-steps.service';

describe('ExerciseCreateStepsService', () => {
  let service: ExerciseCreateStepsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ExerciseCreateStepsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
