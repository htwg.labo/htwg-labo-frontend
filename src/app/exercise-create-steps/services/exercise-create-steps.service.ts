import { Injectable, OnDestroy } from '@angular/core';
import { Exercise, ExerciseManagementCacheService, ExerciseManagementService } from '@shared';
import { map, ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ExerciseCreateStepsService implements OnDestroy{

  private _subs = new Subscription();
  private _lecturerExercise$$ = new ReplaySubject<Exercise|undefined>(1);

  public lecturerExercise$ = this._lecturerExercise$$.asObservable();

  constructor(
    private _exerciseManagementCacheService: ExerciseManagementCacheService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public subscribeToLecturerExercise(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementCacheService.lecturerExercises$
      .pipe(
        map((lecturerExercises)=>{
          return lecturerExercises.find((exercise)=> exercise.id === exerciseId)
        })
      )
      .subscribe((lecturerExercise)=>{
        this._lecturerExercise$$.next(lecturerExercise);
      })
    );
  }
}
