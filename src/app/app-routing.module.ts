import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExerciseCreateStepsComponent } from './exercise-create-steps/components/exercise-create-steps/exercise-create-steps.component';
import { ExerciseInfoComponent } from './exercise-info/components/exercise-info/exercise-info.component';
import { ExerciseSetStateComponent } from './exercise-set-state/components/exercise-set-state/exercise-set-state.component';
import { StepSetStateComponent } from './exercise-set-state/components/step-set-state/step-set-state.component';
import { HomeComponent } from './home/components/home/home.component';
import { LoginComponent } from './login/components/login/login.component';
import { FaqsComponent } from './settings-and-support/components/faqs/faqs.component';
import { SettingsAndSupportComponent } from './settings-and-support/components/settings-and-support/settings-and-support.component';

const routes: Routes = [
  {
    path: '', redirectTo: '/home', pathMatch: 'full'
  },
  { path: 'login', children:[
    {
      path: '',
      component: LoginComponent    
    },
    {
      path: 'faqs',
      component: FaqsComponent    
    },  
  ] },
  { path: 'home', children: [
    {
      path: '',
      component: HomeComponent    
    },
  ]},
  { path: 'exercise', children: [
    {
      path: 'create-steps/:exerciseId',
      component: ExerciseCreateStepsComponent,
    },
    {
      path: 'info/:exerciseId',
      component: ExerciseInfoComponent
    },
    {
      path: 'set-state/:exerciseId',
      children: [
        {
          path: '',
          component: ExerciseSetStateComponent,
        },
        {
          path: ':stepId',
          component: StepSetStateComponent,          
        }
      ]
    },
  ]},
  { path: 'settings-and-support', children: [
    {
      path: '',
      component: SettingsAndSupportComponent  
    },
    {
      path: 'faqs',
      component: FaqsComponent    
    },
  ] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
