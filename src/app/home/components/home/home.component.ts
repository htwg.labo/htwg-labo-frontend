import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ExerciseManagementCacheService, LoadingService, UserManagementCacheService } from '@shared';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  private _subs = new Subscription();

  public lecturerExercises$ = this._exerciseManagementCacheService.lecturerExercises$;
  public studentExercises$ = this._exerciseManagementCacheService.studentExercises$;
  public user$ = this._userManagementCacheService.user$;
  public loading$ = this._loadingService.loading$;

  constructor(
    private _router: Router,
    private _loadingService: LoadingService,
    private _userManagementCacheService: UserManagementCacheService,
    private _exerciseManagementCacheService: ExerciseManagementCacheService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  ngOnInit(): void {
  }

  public navigateToSettingsAndSupport():void {
    this._router.navigate(['/settings-and-support']);
  }

  public navigateToLecturerExercise(exerciseId:number, hasDefinedSteps:boolean){
    if(hasDefinedSteps){
      this._router.navigate(['/exercise/info/' + exerciseId]);
    } else {
      this._router.navigate(['/exercise/create-steps/' + exerciseId]);
    }
  }

  public navigateToStudentExercise(exerciseId:number){
    this._router.navigate(['/exercise/set-state/' + exerciseId]);
  }
}
