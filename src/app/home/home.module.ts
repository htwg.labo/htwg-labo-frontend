import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './components/home/home.component';
import { DividerModule, HeaderModule, LoadingModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';



@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    CommonModule,
    DividerModule,
    HeaderModule,
    LoadingModule,
    TranslateModule
  ]
})
export class HomeModule { }
