import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './components/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { ButtonModule, HeaderModule, InputModule, LoadingModule } from '@ui';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    LoginComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    InputModule,
    ButtonModule,
    BrowserAnimationsModule,
    MatSnackBarModule,
    LoadingModule,
    TranslateModule,
    HeaderModule
  ],
  exports: [
    LoginComponent
  ]
})
export class LoginModule { }
