import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExerciseSetStateComponent } from './components/exercise-set-state/exercise-set-state.component';
import { DividerModule, HeaderModule, LoadingModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';
import { StepSetStateComponent } from './components/step-set-state/step-set-state.component';



@NgModule({
  declarations: [
    ExerciseSetStateComponent,
    StepSetStateComponent
  ],
  imports: [
    CommonModule,
    LoadingModule,
    HeaderModule,
    TranslateModule,
    DividerModule
  ]
})
export class ExerciseSetStateModule { }
