import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Exercise, ExerciseManagementCacheService, ExerciseManagementService, ExerciseStepStatus, LoadingService } from '@shared';
import { BehaviorSubject, map, ReplaySubject, Subscription } from 'rxjs';

@Component({
  selector: 'app-exercise-set-state',
  templateUrl: './exercise-set-state.component.html',
  styleUrls: ['./exercise-set-state.component.scss']
})
export class ExerciseSetStateComponent implements OnInit, OnDestroy{

  private _exerciseId!:number;
  private _subs = new Subscription();
  private _exerciseStepStatuses$$ = new BehaviorSubject<ExerciseStepStatus[]>([]);
  private _exerciseStepStatus$$ = new BehaviorSubject<ExerciseStepStatus | null | undefined>(null);
  private _studentExercise$$ = new ReplaySubject<Exercise | undefined>(1);
  
  public loading$ = this._loadingService.loading$;
  public exerciseStepStatuses$ = this._exerciseStepStatuses$$.asObservable();
  public exerciseStepStatus$ = this._exerciseStepStatus$$.asObservable();
  public studentExercise$ = this._studentExercise$$.asObservable();

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _loadingService: LoadingService,
    private _exerciseManagementService: ExerciseManagementService,
    private _exerciseManagementCacheService: ExerciseManagementCacheService
  ) {}

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  ngOnInit(): void {
    const exerciseId = this._route.snapshot.params['exerciseId'];
    if(exerciseId){
      const id: number = +exerciseId;
      this._exerciseId = id; 
      this._subscribeToStudentExercise(id);
      this._subscribeToExerciseStepStatuses(id);
    }
  }

  public navigateToHome():void{
    this._router.navigate(['/home']);
  }

  public navigateToStepSetState(stepId: string|undefined):void {
    if(stepId){
      this._router.navigate(['/exercise/set-state/' + this._exerciseId + '/' + stepId]);
    }
  }

  private _subscribeToExerciseStepStatuses(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getExerciseStepStatuses(exerciseId).subscribe((exerciseStepStatuses)=>{
        this._exerciseStepStatuses$$.next(exerciseStepStatuses);
      })
    );
  }

  private _subscribeToStudentExercise(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementCacheService.studentExercises$
      .pipe(
        map((studentExercises) => {
          return studentExercises.find((exercixe)=> exercixe.id === exerciseId)
        })
      )
      .subscribe((studentExercise)=>{
        this._studentExercise$$.next(studentExercise);
      })
    );
  }
}
