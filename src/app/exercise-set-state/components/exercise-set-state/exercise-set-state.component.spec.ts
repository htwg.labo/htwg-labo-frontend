import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExerciseSetStateComponent } from './exercise-set-state.component';

describe('ExerciseSetStateComponent', () => {
  let component: ExerciseSetStateComponent;
  let fixture: ComponentFixture<ExerciseSetStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExerciseSetStateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ExerciseSetStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
