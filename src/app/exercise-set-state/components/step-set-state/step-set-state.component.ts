import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Exercise, ExerciseManagementService, ExerciseStepStatus, LoadingService } from '@shared';
import { ExerciseStepStatusEnum } from '@shared';
import { Subscription, BehaviorSubject, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-step-set-state',
  templateUrl: './step-set-state.component.html',
  styleUrls: ['./step-set-state.component.scss']
})
export class StepSetStateComponent implements OnInit, OnDestroy{

  private _exercisId!: number;
  private _subs = new Subscription();
  private _exerciseStepStatuses$$ = new ReplaySubject<ExerciseStepStatus[]>(1);
  private _exerciseStepStatus$$ = new ReplaySubject<ExerciseStepStatus | null | undefined>(1);
  private _studentExercise$$ = new ReplaySubject<Exercise | null>(1);

  public ExerciseStepStatus = ExerciseStepStatusEnum;
  public selectedStatus!: ExerciseStepStatusEnum;
  public loading$ = this._loadingService.loading$;
  public exerciseStepStatuses$ = this._exerciseStepStatuses$$.asObservable();
  public exerciseStepStatus$ = this._exerciseStepStatus$$.asObservable();
  public studentExercise$ = this._studentExercise$$.asObservable();

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _loadingService: LoadingService,
    private _exerciseManagementService: ExerciseManagementService
  ) {}

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  ngOnInit(): void {
    const exerciseId = this._route.snapshot.params['exerciseId'];
    const stepId = this._route.snapshot.params['stepId'];
    if(exerciseId && stepId){
      const id: number = +exerciseId;
      this._exercisId= id;
      this._subscribeToExerciseStepStatuses(id);
      this._subscribeToExerciseStepStatus(id,stepId);
    }
  }

  public navigateToExerciseSetState():void{
    this._router.navigate(['/exercise/set-state/' + this._exercisId]);
  }

  public changeStatus(newStatus: ExerciseStepStatusEnum, oldExerciseStepStatus: any):void{
    const status: ExerciseStepStatus = {
      exerciseStepId: oldExerciseStepStatus.exerciseStepId,
      userId: oldExerciseStepStatus.userId,
      status: newStatus,
      exerciseId: this._exercisId
    }
    this._subscribeToUpdateExerciseStepStatus(status, oldExerciseStepStatus.name, oldExerciseStepStatus.description, oldExerciseStepStatus.order);
  }

  private _subscribeToExerciseStepStatuses(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getExerciseStepStatuses(exerciseId).subscribe((exerciseStepStatuses)=>{
        this._exerciseStepStatuses$$.next(exerciseStepStatuses);
      })
    );
  }

  private _subscribeToExerciseStepStatus(exerciseId:number, stepId: string):void{
    this._subs.add(
      this._exerciseManagementService.getExerciseStepStatus(exerciseId, stepId).subscribe((exerciseStepStatus)=>{
        this._exerciseStepStatus$$.next(exerciseStepStatus);
      })
    );
  }

  private _subscribeToUpdateExerciseStepStatus(exerciseStepStatus: ExerciseStepStatus, name: string, description:string, order: number):void{
    this._subs.add(
      this._exerciseManagementService.updateExerciseStepStatus(exerciseStepStatus, name, description, order).subscribe((exerciseStepStatus)=>{
        this._exerciseStepStatus$$.next(exerciseStepStatus);
      })
    );
  }
}
