import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StepSetStateComponent } from './step-set-state.component';

describe('StepSetStateComponent', () => {
  let component: StepSetStateComponent;
  let fixture: ComponentFixture<StepSetStateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StepSetStateComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StepSetStateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
