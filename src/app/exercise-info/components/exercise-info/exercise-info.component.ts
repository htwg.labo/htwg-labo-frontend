import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Exercise, ExerciseManagementCacheService, ExerciseManagementService, ExerciseStepStatus, ExerciseStepStatusEnum, ExerciseWithSteps, LoadingService, User, UserManagementService } from '@shared';
import { BehaviorSubject, forkJoin, interval, map, of, ReplaySubject, Subscription, switchMap } from 'rxjs';

interface TableEntry{
  participant: User,
  exerciseStepStatuses: ExerciseStepStatus[]
}
@Component({
  selector: 'app-exercise-info',
  templateUrl: './exercise-info.component.html',
  styleUrls: ['./exercise-info.component.scss']
})
export class ExerciseInfoComponent implements OnInit, OnDestroy {

  private _exerciseId!:number;
  private _subs = new Subscription();
  private _lecturerExercise$$ = new ReplaySubject<Exercise | undefined>(1);
  private _exerciseWithSteps$$ = new ReplaySubject<ExerciseWithSteps | null>(1);
  private _tableData$$ = new BehaviorSubject<TableEntry[]>([]);
  private _numberParticipants$$ = new ReplaySubject<number>(1);
  private _numberParticipantsWhoStarted$$ = new ReplaySubject<number>(1);
  private _numberParticipantsWhoFinished$$ = new ReplaySubject<number>(1);
  private _numberParticipantsWithProblems$$ = new ReplaySubject<number>(1);
  private _percentageTotalDone$$ = new ReplaySubject<number>(1);
  private _courseParticipants$$ = new BehaviorSubject<User[]>([]);

  public showDeleteModal = false;
  public lecturerExercise$ = this._lecturerExercise$$.asObservable();
  public exerciseWithSteps$ = this._exerciseWithSteps$$.asObservable();
  public tableData$ = this._tableData$$.asObservable();
  public numberParticipants$ = this._numberParticipants$$.asObservable();
  public numberParticipantsWhoStarted$ = this._numberParticipantsWhoStarted$$.asObservable();
  public numberParticipantsWhoFinished$ = this._numberParticipantsWhoFinished$$.asObservable();
  public numberParticipantsWithProblems$ = this._numberParticipantsWithProblems$$.asObservable();
  public percentageTotalDone$ = this._percentageTotalDone$$.asObservable();
  public courseParticipants$ = this._courseParticipants$$.asObservable();
  public loading$ = this._loadingService.loading$;
  public ExerciseStepStatus = ExerciseStepStatusEnum;

  constructor(
    private _router: Router,
    private _route: ActivatedRoute,
    private _exerciseManagementService: ExerciseManagementService,
    private _exerciseManagementCacheService: ExerciseManagementCacheService,
    private _userManagementService: UserManagementService,
    private _loadingService: LoadingService
  ) { }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  ngOnInit(): void {
    const exerciseId = this._route.snapshot.params['exerciseId'];
    if(exerciseId){
      const id: number = +exerciseId;
      this._exerciseId = id; 
      this._subscribeToLecturerExercise(id);
      this._subscribeToExerciseWithSteps(id);
      this._subscribeToTableData(id);
      this._subscribeToNumberParticipants();
      this._subscribeToNumberParticipantsWhoStarted();
      this._subscribeToNumberParticipantsWithProblems();
      this._subscribeToPercentageTotalDone();
      this._subscribeToNumberParticipantsWhoFinished();
      this._subscribeToCourseParticipants();
      this._subs.add(
          interval(1000 * 10).subscribe( () => {
            this._subscribeToTableData(id); //calls method every 10 seconds (updates table data 10 seconds)
          }
        )
      )
    }
  }

  public navigateToHome():void{
    this._router.navigate(['/home']);
  }

  public deleteExerciseWithSteps(){
    if(this._exerciseId){
      this._exerciseManagementService.deleteExerciseWithSteps(this._exerciseId);
      this._exerciseManagementCacheService.subscribeToLecturerExercises();
    }
  }

  private _subscribeToLecturerExercise(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementCacheService.lecturerExercises$
      .pipe(
        map((lecturerExercises)=>{
          return lecturerExercises.find((exercise)=> exercise.id === exerciseId)
        })
      )
      .subscribe((lecturerExercise)=>{
        this._lecturerExercise$$.next(lecturerExercise);
      })
    );
  }

  private _subscribeToExerciseWithSteps(exerciseId:number):void{
    this._subs.add(
      this._exerciseManagementService.getExerciseWithSteps(exerciseId).subscribe((exerciseWithSteps)=>{
        this._exerciseWithSteps$$.next(exerciseWithSteps);
      })
    );
  }

  private _subscribeToTableData(exerciseId:number):void{
    this._subs.add(
      this.courseParticipants$.pipe(
        switchMap((participants) => {
          if(participants.length > 0){
            const tableData = participants.map((participant) => this._exerciseManagementService.getExerciseStepStatuses(exerciseId,participant.userid).pipe(
              map((exerciseStepStatuses) => {
                  const tableEntry: TableEntry = {
                    participant: participant,
                    exerciseStepStatuses: exerciseStepStatuses
                  }
                  return tableEntry;
              }),
            ));
            return forkJoin(tableData);                    
        }
        else return of([]);  
        })
      )
      .subscribe((tableData)=>{
        this._tableData$$.next(tableData);
      })
    );
  }

  private _subscribeToNumberParticipants():void{
    this._subs.add(
      this.tableData$.pipe(
        map(tableEntries=>{
          return tableEntries.length;
        })
      )
      .subscribe((numberParticipants)=>{
        this._numberParticipants$$.next(numberParticipants);
      })
    )
  }

  private _subscribeToCourseParticipants():void{
    this._subs.add(
      this.lecturerExercise$.pipe(
        switchMap((exercise) => {
          if(exercise){
            return this._userManagementService.getCourseParticipants(exercise.courseid);
          }else{
            return [];
          }
        })
      ).subscribe((courseParticipants)=>{
        this._courseParticipants$$.next(courseParticipants);
      })
    )
  }

  private _subscribeToNumberParticipantsWhoStarted():void{
    this._subs.add(
      this.tableData$.pipe(
        map(tableEntries=>{
          let numberParticipantsWhoStarted = 0;
            tableEntries.forEach(entry =>{
              const numberSteps = entry.exerciseStepStatuses.length;
              let numberTodos = entry.exerciseStepStatuses.filter(status =>{
                return status.status === ExerciseStepStatusEnum.TODO;
              }).length;
              if(numberSteps !== numberTodos){
                numberParticipantsWhoStarted = numberParticipantsWhoStarted + 1;
              }
            })
          return numberParticipantsWhoStarted;
        })
      )
      .subscribe((numberParticipantsWhoStarted)=>{
        this._numberParticipantsWhoStarted$$.next(numberParticipantsWhoStarted);
      })
    )
  }

  private _subscribeToNumberParticipantsWhoFinished():void{
    this._subs.add(
      this.tableData$.pipe(
        map(tableEntries=>{
          let numberParticipantsWhoFinished = 0;
            tableEntries.forEach(entry =>{
              const numberSteps = entry.exerciseStepStatuses.length;
              let numberDones = entry.exerciseStepStatuses.filter(status =>{
                return status.status === ExerciseStepStatusEnum.DONE;
              }).length;
              if(numberSteps === numberDones){
                numberParticipantsWhoFinished = numberParticipantsWhoFinished + 1;
              }
            })
          return (numberParticipantsWhoFinished);
        })
      )
      .subscribe((numberParticipantsWhoFinished)=>{
        this._numberParticipantsWhoFinished$$.next(numberParticipantsWhoFinished);
      })
    )
  }

  private _subscribeToNumberParticipantsWithProblems():void{
    this._subs.add(
      this.tableData$.pipe(
        map(tableEntries=>{
          let numberParticipantsWithProblems = 0;
            tableEntries.forEach(entry =>{
              let numberProblems = entry.exerciseStepStatuses.filter(status =>{
                return status.status === ExerciseStepStatusEnum.PROBLEM;
              }).length;
              if(numberProblems > 0){
                numberParticipantsWithProblems = numberParticipantsWithProblems + 1;
              }
            })
          return numberParticipantsWithProblems;
        })
      )
      .subscribe((numberParticipantsWithProblems)=>{
        this._numberParticipantsWithProblems$$.next(numberParticipantsWithProblems);
      })
    )
  }

  private _subscribeToPercentageTotalDone():void{
    this._subs.add(
      this.tableData$.pipe(
        map(tableEntries=>{
          if(tableEntries.length > 0){
            const numberAllSteps = tableEntries.length * tableEntries[0].exerciseStepStatuses.length;
            let numberAllDones = 0;
              tableEntries.forEach(entry =>{
                let numberDones = entry.exerciseStepStatuses.filter(status =>{
                  return status.status === ExerciseStepStatusEnum.DONE;
                }).length;
                numberAllDones = numberAllDones + numberDones;
              })
              return numberAllDones/numberAllSteps*100;
          }
          return 0;
        })
      )
      .subscribe((percentageTotalDone)=>{
        this._percentageTotalDone$$.next(percentageTotalDone);
      })
    )
  }
}
