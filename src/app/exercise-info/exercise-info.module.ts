import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExerciseInfoComponent } from './components/exercise-info/exercise-info.component';
import { DataStatsModule, HeaderModule, LoadingModule, ModalModule } from '@ui';
import { TranslateModule } from '@ngx-translate/core';
import {MatTableModule} from '@angular/material/table';

@NgModule({
  declarations: [
    ExerciseInfoComponent,
  ],
  imports: [
    CommonModule,
    LoadingModule,
    HeaderModule,
    TranslateModule,
    MatTableModule,
    ModalModule,
    DataStatsModule
  ]
})
export class ExerciseInfoModule { }
