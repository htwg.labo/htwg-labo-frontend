import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsAndSupportComponent } from './settings-and-support.component';

describe('SettingsAndSupportComponent', () => {
  let component: SettingsAndSupportComponent;
  let fixture: ComponentFixture<SettingsAndSupportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SettingsAndSupportComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SettingsAndSupportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
