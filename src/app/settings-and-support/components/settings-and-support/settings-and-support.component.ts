import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AuthService, LoadingService, User, UserManagementCacheService, UserManagementService } from '@shared';
import { Subscription, ReplaySubject } from 'rxjs';

@Component({
  selector: 'app-settings-and-support',
  templateUrl: './settings-and-support.component.html',
  styleUrls: ['./settings-and-support.component.scss']
})
export class SettingsAndSupportComponent {

  private _subs = new Subscription();

  public activeLang!: string;
  public user$ = this._userManagementCacheService.user$;
  public loading$ = this._loadingService.loading$;

  constructor(
    private _translateService: TranslateService,
    private _router: Router,
    private _loadingService: LoadingService,
    private _userManagementCacheService: UserManagementCacheService
  ) {
    this.activeLang = this._translateService.currentLang;
  }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

  public changeLanguageToGerman():void{
    this._translateService.use('de');
    localStorage.setItem ('activeLang', 'de');
    this.activeLang = 'de';
  }

  public changeLanguageToEnglish():void{
    this._translateService.use('en');
    localStorage.setItem ('activeLang', 'en');
    this.activeLang = 'en';
  }

  public navigateToHome():void{
    this._router.navigate(['/home']);
  }

  public navigateToFaqs():void{
    this._router.navigate(['/settings-and-support/faqs']);
  }

  public logout():void{
    localStorage.removeItem('token');
    this._router.navigate(['/login']);
  }
}
