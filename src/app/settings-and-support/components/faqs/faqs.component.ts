import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-faqs',
  templateUrl: './faqs.component.html',
  styleUrls: ['./faqs.component.scss']
})
export class FaqsComponent {

  constructor(
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  public navigateBack():void{
    this._router.navigate(['../'], { relativeTo: this._route });
  }

}
