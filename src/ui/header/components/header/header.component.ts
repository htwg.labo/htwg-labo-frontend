import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  /**
   * Title text of header.
   */
   @Input() title!:string;

  /**
   * Has header back button?
   */
   @Input() hasBackButton = false;

   /**
    * Output of back button is set to true if clicked.
    */
   @Output() backButtonIsClicked = new EventEmitter<boolean>();

  /**
   * Icon name of optional action button.
   */
   @Input() actionButtonIcon!: string;

   /**
    * Output of action button is set to true if clicked.
    */
   @Output() actionButtonIsClicked = new EventEmitter<boolean>();

  /**
   * Image src of user button.
   */
   @Input() userButtonImgSrc!: string | null;

   /**
    * Output of user button is set to true if clicked.
    */
   @Output() userButtonIsClicked = new EventEmitter<boolean>();

  constructor() { }

  public backButtonClicked():void{
    this.backButtonIsClicked.emit(true);
  }

  public actionButtonClicked():void{
    this.actionButtonIsClicked.emit(true);
  }

  public userButtonClicked():void{
    this.userButtonIsClicked.emit(true);
  }
}
