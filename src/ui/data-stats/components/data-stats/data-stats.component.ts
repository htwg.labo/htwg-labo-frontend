import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-data-stats',
  templateUrl: './data-stats.component.html',
  styleUrls: ['./data-stats.component.scss']
})
export class DataStatsComponent {


  /**
   * Value of data stats component.
   */
   @Input() value!: string;

  /**
   * Text of data stats component.
   */
   @Input() text!: string;

  /**
   * Percentage of data stats component.
   */
   @Input() percentage!: number;
   
  constructor() { }

}
