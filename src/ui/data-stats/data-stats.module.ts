import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DataStatsComponent } from './components/data-stats/data-stats.component';



@NgModule({
  declarations: [
    DataStatsComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    DataStatsComponent
  ]
})
export class DataStatsModule { }
