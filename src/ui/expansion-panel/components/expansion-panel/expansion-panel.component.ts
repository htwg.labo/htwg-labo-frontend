import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'ui-expansion-panel',
  templateUrl: './expansion-panel.component.html',
  styleUrls: ['./expansion-panel.component.scss'],
  animations : [
    trigger('animationShowHide', [
      state('false', style({ height: '0px', overflow: 'hidden'})),
      state('true', style({ height: '*',overflow: 'hidden'})),
      transition('true <=> false', animate('500ms ease-in-out')),
    ]),
  ]
})
export class ExpansionPanelComponent {
  /**
   * Title of expansion-panel.
   */
   @Input() title!: string|null;

  /**
   * Content of expansion-panel.
   */
   @Input() content!: string;

  /**
   * Is divider clickable?
   */
   @Input() expanded = false;

  constructor() { }

  public clicked():void{
    this.expanded = !this.expanded;
  }
}
