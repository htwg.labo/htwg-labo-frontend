import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExpansionPanelComponent } from './components/expansion-panel/expansion-panel.component';



@NgModule({
  declarations: [
    ExpansionPanelComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ExpansionPanelComponent
  ]
})
export class ExpansionPanelModule { }
