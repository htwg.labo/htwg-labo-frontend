import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-description-input',
  templateUrl: './description-input.component.html',
  styleUrls: ['./description-input.component.scss']
})
export class DescriptionInputComponent {

  /**
  * Output value of input field.
  */
   @Output() value = new EventEmitter<string>();

   /**
    * Label of input field.
    */
   @Input() label!: string;
 
   /**
    * Has input value an error?
    */
    @Input() error = false;

    /**
    * Placeholder of input field.
    */
      @Input() placeholder = '';

   /**
    * The maximum number of characters allowed in the input description.
    */
    @Input() maxLength = 1000;
 
    public numberCharacters = 0;

   constructor() { }
 
   public valueChanged(event: Event):void{
       this.value.emit((event.target as HTMLInputElement).value);
       this.numberCharacters = (event.target as HTMLInputElement).value.length;
   }
}
