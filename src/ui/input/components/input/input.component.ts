import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss']
})
export class InputComponent {

  /**
  * Output value of input field.
  */
  @Output() value = new EventEmitter<string>();

  /**
   * Label of input field.
   */
  @Input() label!: string;

  /**
   * Is input a password field?
   */
   @Input() password = false;

  /**
   * Has input value an error?
   */
   @Input() error = false;

  constructor() { }

  public valueChanged(event: Event):void{
      this.value.emit((event.target as HTMLInputElement).value);
  }
}
