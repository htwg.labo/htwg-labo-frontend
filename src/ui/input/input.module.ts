import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InputComponent } from './components/input/input.component';
import { FormsModule } from '@angular/forms';
import { ShortInputComponent } from './components/short-input/short-input.component';
import { DescriptionInputComponent } from './components/description-input/description-input.component';



@NgModule({
  declarations: [
    InputComponent,
    ShortInputComponent,
    DescriptionInputComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    InputComponent,
    ShortInputComponent,
    DescriptionInputComponent
  ]
})
export class InputModule { }
