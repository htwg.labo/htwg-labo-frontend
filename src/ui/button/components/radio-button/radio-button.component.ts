import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-radio-button',
  templateUrl: './radio-button.component.html',
  styleUrls: ['./radio-button.component.scss']
})
export class RadioButtonComponent {

  /**
   * Text of radio button.
   */
   @Input() public text!: string;

  /**
   * Active status of radio button.
   */
   @Input() public active: boolean = false;

   /**
    * Output of radio button is set to true if clicked.
    */
   @Output() public isClicked = new EventEmitter<boolean>();
   
  constructor() { }

  public clicked():void{
    this.active = !this.active;
    this.isClicked.emit(true);
  }
}
