import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {

  /**
   * Text of button.
   */
  @Input() public text!: string;

  /**
   * Is button a warning button?
   */
   @Input() public warning = false;

  /**
   * Is button a light button?
   */
   @Input() public light = false;

  /**
   * Output of button is set to true if clicked.
   */
  @Output() public isClicked = new EventEmitter<boolean>();

  constructor() { }

  public clicked():void{
    this.isClicked.emit(true);
  }
}
