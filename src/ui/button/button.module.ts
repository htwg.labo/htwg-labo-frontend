import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonComponent } from './components/button/button.component';
import { RadioButtonComponent } from './components/radio-button/radio-button.component';
import { LightenButtonComponent } from './components/lighten-button/lighten-button.component';



@NgModule({
  declarations: [
    ButtonComponent,
    RadioButtonComponent,
    LightenButtonComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ButtonComponent,
    RadioButtonComponent,
    LightenButtonComponent
  ]
})
export class ButtonModule { }
