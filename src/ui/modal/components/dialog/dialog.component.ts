import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ui-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent {

  /**
   * Title text of dialog.
   */
   @Input() title!:string;

  /**
   * Description text of dialog.
   */
   @Input() description!:string;

  /**
   * Text of abort button.
   */
   @Input() abortButtonText="Abort";

  /**
   * Text of confirm button.
   */
   @Input() confirmButtonText="Confirm";

  /**
   * Is dialog opened?
   */
   @Input() opened = false;

  /**
   * Output of dialog is set to true if confirmed button clicked.
   */
   @Output() confirmed = new EventEmitter<boolean>();

  /**
   * Output of dialog is set to true if dialog is closed.
   */
   @Output() closed = new EventEmitter<boolean>();

  constructor() { }

  public close(): void{
    this.opened = false;
    this.closed.emit(true);
  }

  public confirm(): void{
    this.closed.emit(true);
    this.confirmed.emit(true);
  } 
}
