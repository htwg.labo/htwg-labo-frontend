import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogComponent } from './components/dialog/dialog.component';
import { ButtonModule } from '@ui';



@NgModule({
  declarations: [
    DialogComponent
  ],
  imports: [
    CommonModule,
    ButtonModule
  ],
  exports: [
    DialogComponent
  ]
})
export class ModalModule { }
