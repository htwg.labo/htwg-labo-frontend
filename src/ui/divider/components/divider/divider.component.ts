import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ExerciseStepStatusEnum } from '@shared';

@Component({
  selector: 'ui-divider',
  templateUrl: './divider.component.html',
  styleUrls: ['./divider.component.scss']
})
export class DividerComponent {

  /**
   * Top info text of divider.
   */
   @Input() topInfoText!: string|null;

  /**
   * Center info text of divider.
   */
   @Input() centerInfoText = '';

  /**
   * Bottom info text of divider.
   */
   @Input() bottomInfoText = '';

  /**
   * Status of divider. Bottom info text is not displayed if status is set.
   */
   @Input() status!: ExerciseStepStatusEnum;

  /**
   * Icon of divider.
   */
   @Input() icon!: string;

  /**
   * Is divider clickable?
   */
   @Input() clickable = false;

  /**
   * Is divider selected?
   */
   @Input() selected = false;

  /**
   * Is divider selectable?
   */
   @Input() selectable = false;

  /**
   * Output of button is set to true if clicked.
   */
   @Output() isClicked = new EventEmitter<boolean>();

   public ExerciseStepStatus = ExerciseStepStatusEnum;

  constructor() { }

  public clicked():void{
    this.isClicked.emit(true);
  }

}
